# TP_MN

Ceci est le dépôt des TP de MN portant sur les BLAS1, BLAS2 et BLAS3, ainsi que du TP d'Algorithmique portant sur les graphes pondérées.

Tous les dossiers contiennent leur Makefile, prêt à être compiler.

## Test BLAS
Pour tester les BLAS : 
- placez vous dans le dossier TP3_MN
- puis dans src et faites make 
- les fichiers de dépendances seront générés et puis placez vous dans examples puis lancer make 
- enfin lancer le test que vous souhaitez (./test_complexe par ex)

## Test Graphe
Pour tester les BLAS : 
- placer vous dans le dossier TP3_ALG
- lancez make 
- enfin lancer le test d'un graphe que vous souhaitez (./test_graphe ./data/gr0 par ex pour graphe0)
