#include "filePrio.h"
#include <stdlib.h>
#include <stdio.h>


// Initialisation de la file de priorité
file_priorite* init_file_priorite(int capacite) {
    file_priorite* file = (file_priorite*)malloc(sizeof(file_priorite));
    file->elements = (element_file*)malloc(capacite * sizeof(element_file));
    file->capacite = capacite;
    file->taille = 0;
    return file;
}

// Vérifie si la file de priorité est vide
int file_priorite_est_vide(file_priorite* file) {
    return file->taille == 0;
}

// Vérifie si la file de priorité est pleine
int file_priorite_est_pleine(file_priorite* file) {
    return file->taille == file->capacite;
}

// Ajoute un élément dans la file de priorité
void ajouter_file_priorite(file_priorite* file, psommet_t sommet, int priorite) {
    if (file_priorite_est_pleine(file)) {
        printf("La file de priorite est pleine.\n");
        return;
    }
    int index = file->taille++;
    while (index > 0 && file->elements[(index - 1) / 2].priorite > priorite) {
        file->elements[index] = file->elements[(index - 1) / 2];
        index = (index - 1) / 2;
    }
    file->elements[index].sommet = sommet;
    file->elements[index].priorite = priorite;
}

// Retire l'élément de priorité minimale de la file de priorité
psommet_t retirer_min_file_priorite(file_priorite* file) {
   if (file_priorite_est_vide(file)) {
        printf("La file de priorite est vide.\n");
        return NULL;
    }
    psommet_t min_sommet = file->elements[0].sommet;
    int min_priorite = file->elements[0].priorite;
    int min_index = 0;

    // Trouver l'élément de priorité minimale
    for (int i = 1; i < file->taille; i++) {
        if (file->elements[i].priorite < min_priorite) {
            min_sommet = file->elements[i].sommet;
            min_priorite = file->elements[i].priorite;
            min_index = i;
        }
    }

    // Retirer l'élément de la file à priorité
    file->elements[min_index] = file->elements[file->taille - 1];
    file->taille--;

    // Réorganiser la file à priorité
    int index = min_index;
    while (2 * index + 1 < file->taille) {
        int enfant = 2 * index + 1;
        if (enfant + 1 < file->taille && file->elements[enfant + 1].priorite < file->elements[enfant].priorite)
            enfant++;
        if (file->elements[index].priorite <= file->elements[enfant].priorite)
            break;
        element_file temp = file->elements[index];
        file->elements[index] = file->elements[enfant];
        file->elements[enfant] = temp;
        index = enfant;
    }

    return min_sommet;
}