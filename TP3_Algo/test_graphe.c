#include <stdio.h>
#include <stdlib.h>

#include "graphe.h"

int main (int argc, char **argv)
{
  pgraphe_t g ;
  int nc ;
  
  if (argc != 2)
    {
      fprintf (stderr, "erreur parametre \n") ;
      exit (-1) ;
    }

  /*
    la fonction lire_graphe alloue le graphe (matrice,...) 
    et lit les donnees du fichier passe en parametre
  */
  
  
  lire_graphe (argv [1], &g) ;

  /*
    la fonction ecrire_graphe affiche le graphe a l'ecran
  */  
  
  printf ("nombre de sommets du graphe %d nombre arcs %d \n", nombre_sommets (g), nombre_arcs (g)) ;
  fflush (stdout) ;
  
  ecrire_graphe (g) ;      

  nc = colorier_graphe (g) ;
  
  printf ("nombre chromatique graphe = %d\n", nc) ;

  ecrire_graphe_colorie (g) ;

  printf("\nParcours en largeur\n");

  afficher_graphe_largeur(g,g->label);

  printf("\nParcours en profondeur\n");

  afficher_graphe_profondeur(g,g->label);

  printf("\nDijkstra (%d)\n" , g->label);

  algo_dijkstra(g,g->label);
  afficher_dijkstra(g);

  printf("\ndegret sortant sommet :\n");
  psommet_t s = (psommet_t) g;
  while(s!= NULL){
    printf("degre sortant de %d : %d \n",s->label , degre_sortant_sommet(g,s) );
    s = s->sommet_suivant;
  }

  printf("\n\ndegres entrant sommet :\n");
  s = (psommet_t) g;
  while(s!= NULL){
    printf("degre entrant de %d : %d \n",s->label , degre_entrant_sommet(g,s) );
    s = s->sommet_suivant;
  }

  printf("\nle degree maximal du graph est : %d\n", degre_maximal_graphe(g));

  printf("le degree minimal du graph est : %d\n\n", degre_minimal_graphe(g));

  if(independant(g)){
    printf("ce graph EST indépendant\n");
  }else{
    printf("ce graphe N'EST PAS independant\n");
  }

  if(complet(g)){
    printf("ce graph EST complet\n");
  }else{
    printf("ce graphe N'EST PAS complet\n");
  }

  if(regulier(g)){
    printf("ce graph EST regulier\n");
  }else{
    printf("ce graphe N'EST PAS regulier\n");
  }
  printf("Distance entre 0 et 8 du graphe0 doit être (6)  : %d \n", distance(g, g->label, 8));

  // test aléatoire pour voir si les erreures et les gestions mémoire sont bien réglées

  printf("########################\n");
  printf("Test du parcours en largeur, à partir du sommet 0\n");
  afficher_graphe_largeur(g,0);

  printf("########################\n");
  printf("Test du parcours en profondeur, à partir du sommet 5\n");
  afficher_graphe_profondeur(g,5);

  printf("########################\n");
  printf("Test du parcours de recherche de Djikstra, à partir du sommet 1\n");
  algo_dijkstra(g,1);
  afficher_dijkstra(g);
}



