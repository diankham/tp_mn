/*
Structures de type graphe
Structures de donnees de type liste
(Pas de contrainte sur le nombre de noeuds des  graphes)
*/


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#ifndef graphe_h
#define graphe_h
#include "graphe.h"
#endif

#include "file.h"
#include "pile.h"
#include "filePrio.h"


psommet_t chercher_sommet (pgraphe_t g, int label)
{
  psommet_t s ;

  s = g ;

  while ((s!= NULL) && (s->label != label))
    {
      s = s->sommet_suivant ;
    }
  return s ;
}

parc_t existence_arc (parc_t l, psommet_t s)
{
  parc_t p = l ;

  while (p != NULL)
    {
      if (p->dest == s)
	return p ;
      p = p->arc_suivant ;
    }
  return p ;
  
}


void ajouter_arc (psommet_t o, psommet_t d, int distance)
{
  parc_t parc ;

  parc = (parc_t) malloc (sizeof(arc_t)) ;

  if (existence_arc (o->liste_arcs, d) != NULL)
    {
      fprintf(stderr, "ajout d'un arc deja existant\n") ;
      exit (-1) ;
    }
  
  parc->poids = distance ;
  parc->dest = d ;
  parc->arc_suivant = o->liste_arcs ;
  o->liste_arcs = parc ;
  return ;
}



// ===================================================================

int nombre_sommets (pgraphe_t g)
{
  psommet_t p = g ;
  int nb = 0 ;

  while (p != NULL)
    {
      nb = nb + 1 ;
      p = p->sommet_suivant ;
    }

  return nb ;
}

int nombre_arcs (pgraphe_t g)
{

  psommet_t p = g ;
  int nb_arcs = 0 ;

  while (p != NULL)
    {
      parc_t l = p->liste_arcs ;

      while (l != NULL)
	{
          nb_arcs = nb_arcs + 1 ;
	  l = l->arc_suivant ;
	}
      
      p = p->sommet_suivant ;
    }
  return nb_arcs ;
}

void init_couleur_sommet (pgraphe_t g)
{
  psommet_t p = g ;

  while (p != NULL)
    {
      p->couleur = 0 ; // couleur indefinie
      p = p->sommet_suivant ; // passer au sommet suivant dans le graphe
    }
  
  return ;
}

int colorier_graphe (pgraphe_t g)
{
  /*
    coloriage du graphe g
    
    datasets
    graphe data/gr_planning
    graphe data/gr_sched1
    graphe data/gr_sched2
  */

  psommet_t p = g ;
  parc_t a ;
  int couleur ;
  int max_couleur = INT_MIN ; // -INFINI
  
  int change ;

  init_couleur_sommet (g) ;
  
  while (p != NULL)
    {
      couleur = 1 ; // 1 est la premiere couleur

      // Pour chaque sommet, on essaie de lui affecter la plus petite couleur

      // Choix de la couleur pour le sommet p
      
      do
	{
	  a = p->liste_arcs ;
	  change = 0 ;
      
	  while (a != NULL)
	    {
	      if (a->dest->couleur == couleur)
		{
		  couleur = couleur + 1 ;
		  change = 1 ;
		} 
	      a = a->arc_suivant ; 
	    }

	} while (change == 1) ;

      // couleur du sommet est differente des couleurs de tous les voisins
      
      p->couleur = couleur ;
      if (couleur > max_couleur)
	max_couleur = couleur ;

      p = p->sommet_suivant ;
    }
  
  return max_couleur ;
}

// Affiche les sommets du graphe en largeur à partir du sommet initial r
void afficher_graphe_largeur(pgraphe_t g, int r) {
    if (g == NULL) {
        printf("graphe null\n");
        return;
    }
    psommet_t g2 = chercher_sommet(g, r);
    if (g2 == NULL) {
        printf("Sommet non présent dans le graphe ou null\n");
        return;
    }

    init_couleur_sommet(g); // Initialise les couleurs des sommets pour marquer les visites

    pfile_t file = creer_file();
    enfiler(file, g2); // Ajoute le sommet initial à la file
    printf(" %d ,", g2->label); // Affiche le sommet initial
    g2->couleur = 1; // Marque le sommet initial comme visité
    while (!file_vide(file)) {
        psommet_t sommet = defiler(file);
        parc_t arcs = sommet->liste_arcs;
        while (arcs) {
            if (arcs->dest->couleur == 0) { // Si le sommet n'a pas encore été visité
                printf(" %d ,", arcs->dest->label); // Affiche le sommet
                enfiler(file, arcs->dest); // Ajoute le sommet à la file
                arcs->dest->couleur = 1; // Marque le sommet comme visité
            }
            arcs = arcs->arc_suivant;
        }
    }
    detruire_file(file);
    printf("\n");
    return;
}

// Affiche les sommets du graphe en profondeur à partir du sommet initial r
void afficher_graphe_profondeur(pgraphe_t g, int r) {
    if (g == NULL) {
        printf("graphe null\n");
        return;
    }
    psommet_t g2 = chercher_sommet(g, r);
    if (g2 == NULL) {
        printf("Sommet non présent dans le graphe ou null\n");
        return;
    }
    init_couleur_sommet(g); // Initialise les couleurs des sommets pour marquer les visites

    ppile_t pile = creer_pile();
    empiler(pile, g2); // Ajoute le sommet initial à la pile
    while (!pile_vide(pile)) {
        psommet_t sommet = depiler(pile);
        if (sommet->couleur == 0) { // Si le sommet n'a pas encore été visité
            printf(" %d ,", sommet->label); // Affiche le sommet
            sommet->couleur = 1; // Marque le sommet comme visité
        }
        parc_t arcs = sommet->liste_arcs;
        while (arcs) {
            if (arcs->dest->couleur == 0) { // Si le sommet destination n'a pas encore été visité
                empiler(pile, sommet); // Empile le sommet actuel
                empiler(pile, arcs->dest); // Empile le sommet destination
                break;
            }
            arcs = arcs->arc_suivant;
        }
    }
    detruire_pile(pile);
    printf("\n");
    return;
}


// Algorithme de Dijkstra pour trouver les plus courts chemins depuis un sommet source
void algo_dijkstra(pgraphe_t g, int r) {
    if (g == NULL) {
        printf("graphe Null\n");
        return;
    }
    // Initialisation des distances et prédécesseurs
    psommet_t s = chercher_sommet(g, r);
    if (s == NULL) {
        printf("sommet non présent\n");
        return;
    }
    // Initialisation des distances et prédécesseurs
    for (psommet_t v = g; v != NULL; v = v->sommet_suivant) {
        v->sommet_precedant_dijkstra = NULL;
        if (v == s) {
            v->couleur = 0; // Couleur de s
            v->distance = 0; // Distance de s à s est 0
        } else {
            v->couleur = 1; // Couleur des autres sommets
            v->distance = INT_MAX; // Initialisation à l'infini
        }
    }
    // Initialisation de la file de priorité
    file_priorite* file = init_file_priorite(100); // Choisir une capacité appropriée
    // Ajout du sommet source à la file de priorité
    ajouter_file_priorite(file, s, 0);
    // boucle de l'algorithme de Dijkstra
    while (!file_priorite_est_vide(file)) {
        psommet_t v = retirer_min_file_priorite(file);
        for (parc_t arc = v->liste_arcs; arc != NULL; arc = arc->arc_suivant) {
            psommet_t w = arc->dest;
            if (w->couleur == 1) { // Si w n'est pas encore exploré
                int dvw = arc->poids;
                if (v == w && dvw < w->distance) { // Gestion de la boucle
                    w->distance = dvw;
                    w->sommet_precedant_dijkstra = v;
                    ajouter_file_priorite(file, w, dvw);
                } else if (v->distance + dvw < w->distance) {
                    w->distance = v->distance + dvw; // Mise à jour de la distance
                    w->sommet_precedant_dijkstra = v;
                    ajouter_file_priorite(file, w, w->distance);
                }
            }
        }
        v->couleur = 2; // Marquer v comme exploré
    }
    // Libération de la mémoire allouée pour la file de priorité
    free(file->elements);
    free(file);
    return;
}

// Affiche les résultats de l'algorithme de Dijkstra
void afficher_dijkstra(pgraphe_t g) {
    if (g == NULL) {
        printf("Erreur ! graphe Null\n");
        return;
    }
    while (g != NULL) {
        if (g->sommet_precedant_dijkstra == NULL) {
            printf("le sommet %d n'est pas accessible\n", g->label);
        } else {
            printf("le sommet %d est accessible par le sommet %d avec un poids de %d\n", g->label, g->sommet_precedant_dijkstra->label, g->distance);
        }
        g = g->sommet_suivant;
    }
}




// ======================================================================




int degre_sortant_sommet (pgraphe_t g, psommet_t s){
  /*
  Cette fonction retourne le nombre d'arcs sortants
  du sommet s dans le graphe g
  */
   if(g == NULL || s == NULL){
    printf("Erreur ! graphe Null\n");
    return -1;
  }
  int count = 0;
  parc_t arcs = s->liste_arcs;
  while(arcs != NULL){
    count ++;
    arcs = arcs->arc_suivant;
  }
  return count ;
}

int degre_entrant_sommet (pgraphe_t g, psommet_t s){
  /*
  Cette fonction retourne le nombre d'arcs entrants
  dans le sommet s dans le graphe g
  */

  if(g == NULL || s == NULL){
    printf("Erreur ! graphe Null\n");
    return -1;
  }
  int count = 0;
  psommet_t g2 = (psommet_t)g ;
  while(g2 != NULL){
    parc_t arcs = g2->liste_arcs;
    while(arcs != NULL){
      if(arcs->dest == s){
        count ++;
      }
      arcs = arcs->arc_suivant;
    }
    g2 = g2->sommet_suivant;
  }
  return count ;
}

int degre_maximal_graphe (pgraphe_t g){
  /*
  Max des degres des sommets du graphe g
  */
  if(g == NULL){
    printf("Erreur ! graphe Null");
    return -1;
  }

  psommet_t g2 = (psommet_t)g;
  int max = INT_MIN;
  while (g2!=NULL) {
    int total_degre = degre_sortant_sommet(g,g2) + degre_entrant_sommet(g,g2);
    if(total_degre > max)
      max= total_degre;
    g2=g2->sommet_suivant;
  }
  return max;
}

int degre_minimal_graphe (pgraphe_t g){
  /*
  Min des degres des sommets du graphe g
  */
  if(g == NULL){
    printf("Erreur ! graphe Null\n");
    return -1;
  }

  psommet_t g2 = (psommet_t)g;
  int min = INT_MAX;
  while (g2!=NULL) {
    int total_degre = degre_sortant_sommet(g,g2) + degre_entrant_sommet(g,g2);
    if(total_degre < min)
      min= total_degre;
    g2=g2->sommet_suivant;
  }
  return min;
}

int sommet_joint(psommet_t s1 , psommet_t s2){
  // retourne vrais si les 2 sommets sont liée dans les 2 sens
  if(s1 == NULL || s2 == NULL){
    printf("Erreur ! graphes Null\n");
    return -1;
  }

  parc_t arc1 = s1->liste_arcs;
  parc_t arc2 = s2->liste_arcs;
  char s1_to_s2 = 0;
  while(arc1!=NULL && !s1_to_s2){
    if(arc1->dest == s2){
      s1_to_s2 = 1;
    }
    arc1 = arc1->arc_suivant;
  }
  if(!s1_to_s2){
    return 0;
  }
  while(arc2 != NULL){
    if(arc2->dest == s1){
      return 1;
    }
    arc2 = arc2->arc_suivant;
  }
  return 0;
}

int complet (pgraphe_t g){
  /* Toutes les paires de sommet du graphe sont jointes par un arc */
  if(g == NULL){
    printf("Erreur ! graphe Null\n");
    return -1;
  }
  psommet_t g1 = (psommet_t)g;
  psommet_t g2 ;
  while(g1!= NULL){
    g2 = g1->sommet_suivant;
    while(g2!=NULL){
      if(!sommet_joint(g1,g2)){
        return 0;
      }
      g2 = g2->sommet_suivant;
    }
    g1=g1->sommet_suivant;
  }
  return 1 ;
}

int regulier (pgraphe_t g){

  /* 
     graphe regulier: tous les sommets ont le meme degre
     g est le ponteur vers le premier sommet du graphe
     renvoie 1 si le graphe est régulier, 0 sinon
  */
  return degre_minimal_graphe(g) == degre_maximal_graphe(g) ;
}


int independant (pgraphe_t g){
  /* Les aretes du graphe n'ont pas de sommet en commun */
  return degre_maximal_graphe(g) <= 1;
}



int nb_sommet_chemin (pchemin_t c){
  // part du principe que un chemin n'est pas infinie
  if(c == NULL){
    printf("Erreur ! chemin non présent\n");
    return -1;
  }
  parc_t arcs = c->liste_arcs;
  int count = 1;
  while(arcs != NULL){
    count ++;
    arcs=arcs->arc_suivant;
  }
  return count ;
}


int elementaire (pgraphe_t g , pchemin_t c ){

  /*
    *Un chemin élémentaire est un chemin ne passant pas deux fois par un même sommet,
    *c’est à dire un chemin dont tous les sommets sont distincts.
  */

  if(g == NULL || c == NULL){
    printf("Erreur ! graphe Null ou chemin non présent\n");
    return -1;
  }

  psommet_t * liste_graphe_chemin = malloc(nb_sommet_chemin(c) * sizeof(psommet_t));
  liste_graphe_chemin[0] = c->depart;
  int nb_actuel = 1;
  parc_t arcs = c->liste_arcs;
  while(arcs!=NULL){
    for(int i = 0; i<nb_actuel ; i++){
      if(liste_graphe_chemin[i] == arcs->dest){
        free(liste_graphe_chemin);
        return 0;
      }
    }
    liste_graphe_chemin[nb_actuel] = arcs->dest;
    nb_actuel++;
    arcs = arcs->arc_suivant;
  }
  free(liste_graphe_chemin);
  return 1;
}



int simple ( pgraphe_t g , pchemin_t c ){

  /*
    * Un chemin simple est un chemin ne passant pas deux fois par le même arc, c’est à dire
    *un chemin dont tous les arcs sont distincts.
  */

  if(g == NULL || c == NULL){
    printf("Erreur ! graphe Null ou chemin non présent\n");
    return -1;
  }

  int nb_arcs = nb_sommet_chemin(c)-1 ;
  int nb_arc_actuel = 1;
  parc_t * liste_arc_chemin =malloc(nb_arcs * sizeof(parc_t));
  liste_arc_chemin[0] = c->liste_arcs;
    parc_t arcs = c->liste_arcs->arc_suivant;
  while(arcs!=NULL){
    for(int i = 0; i < nb_arc_actuel ; i++){
      if(liste_arc_chemin[i] == arcs ){
        free(liste_arc_chemin);
        return 0;
      }
    }
    liste_arc_chemin[nb_arc_actuel] = arcs;
    nb_arc_actuel++;
    arcs = arcs->arc_suivant;
  }
  free(liste_arc_chemin);
  return 1;
}


int eulerien ( pgraphe_t g , pchemin_t c ){

  /*
    * Un chemin est dit Eulérien si tous les arcs du graphe sont utilisés dans le chemin.
    *Un graphe est dit Eulérien si il existe au moins un chemin qui soit Eulérien
  */
  if (g == NULL || c == NULL) {
        printf("Erreur ! Graphe ou chemin non présent\n");
        return -1;
    }
    int nb_arcs_graphe = nombre_arcs(g);
    int nb_arcs_chemin = 0;
    psommet_t sommet = g;
    while (sommet != NULL) {
        parc_t arc = sommet->liste_arcs;
        while (arc != NULL) {
            nb_arcs_chemin++;
            arc = arc->arc_suivant;
        }
        sommet = sommet->sommet_suivant;
    }
    return (nb_arcs_chemin == nb_arcs_graphe);
}


int hamiltonien ( pgraphe_t g , pchemin_t c ){
  /*
    *Un chemin est dit Hamiltonien si tous les sommets du graphe sont utilisés dans le
    *chemin.
    *. Un graphe est dit Hamiltonien si il existe au moins un chemin qui soit Hamiltonien
  */
  if (g == NULL || c == NULL) {
        printf("Erreur ! Graphe ou chemin non présent\n");
        return -1;
    }
    int nb_sommets_chemin = 0;
    psommet_t sommet = g;
    while (sommet != NULL) {
        nb_sommets_chemin++;
        sommet = sommet->sommet_suivant;
    }
    return (nb_sommets_chemin == nombre_sommets(g));
}


int distance ( pgraphe_t g , int x , int y ){
  
  /*
    *La distance entre deux sommets x et y est la longueur du plus court chemin entre x et
    *y.
  */

  // algo de Djikstra qui donne le court chemin à partir de x
  if(g == NULL){
    printf("Erreur ! graphe null\n");
    return -1;
  }

  psommet_t sommeT = chercher_sommet(g,x);
  psommet_t sommeT1 = chercher_sommet(g,y);

  if(sommeT == NULL || sommeT1 == NULL){
    printf("Erreur Sommet n'est pas dans le graphe\n");
    return -1;
  }

  algo_dijkstra(g,x);
  sommeT1 = chercher_sommet(g,y);
  return sommeT1->distance;
}


int excentricite ( pgraphe_t g , int n )
{
  /*
    *L’excentricité d’un sommet est sa distance maximale avec les autres sommets du graphe
  */

  if(g == NULL){
    printf("Erreur ! graphe null\n");
    return -1;
  }

  psommet_t sommeT = chercher_sommet(g,n);
  if(sommeT == NULL){
    printf("Erreur Sommet n'est pas dans le graphe\n");
    return -1;
  }

  algo_dijkstra(g,n);
  psommet_t s = g ;
  int max = INT_MIN;
  while(s != NULL){
    if(s->distance > max){
      max = s->distance;
    }
    s = s->sommet_suivant;
  }
  return max ;
}

int diametre ( pgraphe_t g )
{
  /*
    *Le diamètre d’un graphe est l’excentricité maximale de ses sommets.
  */
  if(g == NULL){
    printf("Erreur ! graphe null\n");
    return -1;
  }
  psommet_t s = g ;
  int max = INT_MIN;
  while(s!=NULL){
    int tmp = excentricite(g,s->label);
    if(tmp> max){
      max = tmp;
    }
  }
  return max ;
}
