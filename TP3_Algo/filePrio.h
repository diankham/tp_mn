#ifndef graphe_h
#define graphe_h
#include "graphe.h"
#endif


// File de priorité
typedef struct {
    psommet_t sommet;
    int priorite;
} element_file;

typedef struct {
    element_file *elements;
    int capacite;
    int taille;
} file_priorite;

int file_priorite_est_vide(file_priorite* file) ;

file_priorite* init_file_priorite(int capacite);

int file_priorite_est_pleine(file_priorite* file);

void ajouter_file_priorite(file_priorite* file, psommet_t sommet, int priorite);

psommet_t retirer_min_file_priorite(file_priorite* file);

