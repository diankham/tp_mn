#include <stdlib.h>
#include <stdio.h>
#include "file.h"

pfile_t creer_file ()
{
  pfile_t f = malloc (sizeof(file_t));
  for (int i = 0; i <MAX_FILE_SIZE; i++){
    f->Tab[i] = NULL;
  }
  f->tete = MAX_FILE_SIZE -1;
  f->queue = MAX_FILE_SIZE -1;
  return f;
}

int detruire_file (pfile_t f)
{
  free(f);
  return 1;
}

int file_vide (pfile_t f)
{
  return ( (f->tete == f->queue) && (f->Tab[f->tete] == NULL) );
}

int file_pleine (pfile_t f)
  {
  return ( (f->tete == f->queue) && (f->Tab[f->tete] != NULL) );
}

psommet_t defiler (pfile_t f)
{
  if(!file_vide(f)){
    psommet_t n = f->Tab[f->queue];
    f->Tab[f->queue] = NULL;
    (f->queue ==0) ? f->queue=MAX_FILE_SIZE -1 : f->queue --;
    return n;
  }else{
    return NULL;
  }
}

int enfiler (pfile_t f, psommet_t p)
{
  if(!file_pleine(f)){
    f->Tab[f->tete] = p;
    (f->tete ==0) ? f->tete=MAX_FILE_SIZE-1 : f->tete --;
    return 1;
  }else{
    return 0;
  }
}
