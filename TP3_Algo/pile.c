#include <stdlib.h>
#include "pile.h"

ppile_t creer_pile ()
{
  ppile_t p = malloc (sizeof(pile_t));
  p->sommet = 0;
  return p ;
}

int detruire_pile (ppile_t p)
{
  free(p);
  return 1;
}

int pile_vide (ppile_t p)
{
  return p->sommet == 0;
}

int pile_pleine (ppile_t p)
 {
  return p->sommet == MAX_PILE_SIZE;
}

psommet_t depiler (ppile_t p)
{
  if (p->sommet ==0){
    return NULL;
  }else {
    psommet_t n = p->Tab[p->sommet-1];
    p->sommet --;
    return n;
  }
}

int empiler (ppile_t p, psommet_t pn)
  {
  return ((p->sommet == MAX_PILE_SIZE) ? 0 : (p->Tab[p->sommet++] = pn) == pn);
}
