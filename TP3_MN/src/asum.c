#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"



float mnblas_sasum ( const int N , const float *X , const int incX )
  {
    register unsigned int i = 0 ;
    register float res = 0.0;

    for(; i<N; i+=incX){
      res += abs_float(X[i]) ;
    }
    return res;
  }


float mnblas_scasum ( const int N , const void *X , const int incX )
  {
    register unsigned int i = 0 ;
    register float res = 0.0;
    register complexe_float_t * X2 = (complexe_float_t*) X;

    for(; i<N; i+=incX){
      res += abs_float(X2[i].real) + abs_float(X2[i].imaginary);
    }
    return res ;
  }


double mnblas_dasum ( const int N , const double *X , const int incX )
  {
    register unsigned int i = 0 ;
    register double res = 0.0;

    for(; i<N; i+=incX){
      res += abs_double(X[i]) ;
    }
    return res ;
  }


double mnblas_dzasum ( const int N , const void *X , const int incX )
  {
    register unsigned int i = 0 ;
    register double res = 0.0;
    register complexe_double_t * X2 = (complexe_double_t*) X;

    for(; i<N; i+=incX){
      res += abs_double(X2[i].real) + abs_double(X2[i].imaginary);
    }
    return res ;
  }
