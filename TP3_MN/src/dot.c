#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"


float mncblas_sdot(const int N, const float *X, const int incX,
                 const float *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register float dot = 0.0 ;

  for (; i < N ; i += incX , j+=incY)
    {
      dot += X [i] * Y [j] ;
    }

  return dot ;
}

double mncblas_ddot(const int N, const double *X, const int incX,
                 const double *Y, const int incY)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register double dot = 0.0 ;

  for (; i < N ; i += incX , j+=incY)
    {
      dot += X [i] * Y [j] ;
    }
  return dot ;
}

void   mncblas_cdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_float_t * X2 = (complexe_float_t*) X;
  register complexe_float_t * Y2 = (complexe_float_t*) Y;
  register complexe_float_t * dot = (complexe_float_t* ) dotu;

  for (; i < N ; i += incX, j+=incY)
    {
      *dot = add_complexe_float(*dot , mult_complexe_float(X2[i] , Y2[j]))  ;
    }
  return ;
}

void   mncblas_cdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_float_t * X2 = (complexe_float_t*) X;
  register complexe_float_t * Y2 = (complexe_float_t*) Y;
  register complexe_float_t * dot = (complexe_float_t* ) dotc;

  for (; i < N ; i += incX ,j+=incY)
    {
      complexe_float_t X_conjuge = {X2[i].real, -X2[i].imaginary} ;
      *dot = add_complexe_float(*dot , mult_complexe_float(X_conjuge , Y2[j]))  ;
    }
  return ;
}

void   mncblas_zdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_double_t * X2 = (complexe_double_t*) X;
  register complexe_double_t * Y2 = (complexe_double_t*) Y;
  register complexe_double_t * dot = (complexe_double_t* ) dotu;

  for (; i < N ; i += incX, j+=incY)
    {
      *dot = add_complexe_double(*dot , mult_complexe_double(X2[i] , Y2[j]))  ;
    }
  return ;
}

void   mncblas_zdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc)
{
  register unsigned int i = 0 ;
  register unsigned int j = 0 ;
  register complexe_double_t * X2 = (complexe_double_t*) X;
  register complexe_double_t * Y2 = (complexe_double_t*) Y;
  register complexe_double_t * dot = (complexe_double_t* ) dotc;

  for (; i < N ; i += incX, j+=incY)
    {
      complexe_double_t X_conjuge = {X2[i].real, -X2[i].imaginary} ;
      *dot = add_complexe_double(*dot , mult_complexe_double(X_conjuge , Y2[j]))  ;
    }
  return ;
}
