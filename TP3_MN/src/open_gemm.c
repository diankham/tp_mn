#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"



void mncblas_sgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc)
{
  // A, B, C sont des matrices de M*M
  register unsigned int i,j,k;

  #pragma omp parallel for
  for(i=0;i<N*N;i+=1){
    C[i] = C[i]*beta;
  }

  #pragma omp parallel for
  for(i=0;i<N;i+=1){
    for(k=0;k<N;k+=1){
      for(j=0;j<N;j+=1){
        C[i*N+j] += A[i*N+k] * B[k*N+j]* alpha;
      }
    }
  }
}

void mncblas_dgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc)
{
  register unsigned int i,j,k;

  #pragma omp parallel for
  for(i=0;i<N*N;i+=1){
    C[i] = C[i]*beta;
  }

  #pragma omp parallel for
  for(i=0;i<N;i+=1){
    for(k=0;k<N;k+=1){
      for(j=0;j<N;j+=1){
        C[i*N+j] += A[i*N+k] * B[k*N+j]* alpha;
      }
    }
  }
  //matr_A_B_alpha = A*B*alpha
}

void mncblas_cgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
  register complexe_float_t * A2 = (complexe_float_t*) A;
  register complexe_float_t * B2 = (complexe_float_t*) B;
  register complexe_float_t * C2 = (complexe_float_t*) C;
  complexe_float_t alpha2 = *((complexe_float_t*) alpha);
  complexe_float_t beta2 = *((complexe_float_t*) beta);

  register unsigned int i,j,k;

  #pragma omp parallel for
  for(i=0;i<N*N;i+=1){
    C2[i] =mult_complexe_float(C2[i], beta2); // C = C*beta
  }

  #pragma omp parallel for
  for(i=0;i<N;i+=1){
    for(k=0;k<N;k+=1){
      for(j=0;j<N;j+=1){
        C2[i*N+j] = add_complexe_float(C2[i*N+j] ,mult_complexe_float(mult_complexe_float(A2[i*N+k] ,B2[k*N+j]), alpha2));
      }
    }
  }
}

void mncblas_zgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc)
{
  register complexe_double_t * A2 = (complexe_double_t*) A;
  register complexe_double_t * B2 = (complexe_double_t*) B;
  register complexe_double_t * C2 = (complexe_double_t*) C;
  complexe_double_t alpha2 = *((complexe_double_t*) alpha);
  complexe_double_t beta2 = *((complexe_double_t*) beta);

  register unsigned int i,j,k;

  #pragma omp parallel for
  for(i=0;i<N*N;i+=1){
    C2[i] =mult_complexe_double(C2[i], beta2); // C = C*beta
  }

  #pragma omp parallel for
  for(i=0;i<N;i+=1){
    for(k=0;k<N;k+=1){
      for(j=0;j<N;j+=1){
        C2[i*N+j] = add_complexe_double(C2[i*N+j] ,mult_complexe_double(mult_complexe_double(A2[i*N+k] ,B2[k*N+j]), alpha2));
      }
    }
  }
}
