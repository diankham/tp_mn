#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"
#include <omp.h>


float mncblas_sdot_open(const int N, const float *X, const int incX,
                 const float *Y, const int incY)
{
  register unsigned int i ;
  register float dot = 0.0 ;

  #pragma omp parallel for reduction(+:dot)
  for (i=0; i < N ; i += incX)
    {
      dot += X [i] * Y [i] ;
    }

  return dot ;
}

double mncblas_ddot_open(const int N, const double *X, const int incX,
                 const double *Y, const int incY)
{
  register unsigned int i ;
  register double dot = 0.0 ;

  #pragma omp parallel for reduction(+:dot)
  for (i=0; i < N ; i += incX)
    {
      dot += X [i] * Y [i] ;
    }
  return dot ;
}

void   mncblas_cdotu_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu)
{
  register unsigned int i ;
  register complexe_float_t * X2 = (complexe_float_t*) X;
  register complexe_float_t * Y2 = (complexe_float_t*) Y;
  register complexe_float_t * dot = (complexe_float_t* ) dotu;

  complexe_float_t c_tmp ;
  float real = 0.0;
  float imm =0.0;

  #pragma omp parallel for reduction(+:real) reduction(+:imm)
  for (i=0; i < N ; i += incX)
    {
      c_tmp = mult_complexe_float(X2[i] , Y2[i]) ;
      real += c_tmp.real ;
      imm += c_tmp.imaginary;
    }
    dot->real = real ;
    dot->imaginary = imm;
  return ;
}


void   mncblas_cdotc_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc)
{
  register unsigned int i ;
  register complexe_float_t * X2 = (complexe_float_t*) X;
  register complexe_float_t * Y2 = (complexe_float_t*) Y;
  register complexe_float_t * dot = (complexe_float_t* ) dotc;

  complexe_float_t c_tmp ;
  float real = 0.0;
  float imm =0.0;

  #pragma omp parallel for reduction(+:real) reduction(+:imm)
  for (i=0; i < N ; i += incX )
    {
      complexe_float_t X_conjuge = {X2[i].real, -X2[i].imaginary} ;
      //*dot = add_complexe_float(*dot , mult_complexe_float(X_conjuge , Y2[i]))  ;
      c_tmp = mult_complexe_float(X_conjuge , Y2[i]) ;
      real += c_tmp.real ;
      imm += c_tmp.imaginary;
    }
    dot->real = real ;
    dot->imaginary = imm;
  return ;
}

void   mncblas_zdotu_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu)
{
  register unsigned int i ;
  register complexe_double_t * X2 = (complexe_double_t*) X;
  register complexe_double_t * Y2 = (complexe_double_t*) Y;
  register complexe_double_t * dot = (complexe_double_t* ) dotu;
  complexe_double_t c_tmp ;
  double real = 0.0;
  double imm =0.0;

  #pragma omp parallel for reduction(+:real) reduction(+:imm)
  for (i=0; i < N ; i += incX)
    {
      c_tmp = mult_complexe_double(X2[i] , Y2[i]) ;
      real += c_tmp.real ;
      imm += c_tmp.imaginary;
    }
    dot->real = real ;
    dot->imaginary = imm;
  return ;
}

void   mncblas_zdotc_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc)
{
  register unsigned int i ;
  register complexe_double_t * X2 = (complexe_double_t*) X;
  register complexe_double_t * Y2 = (complexe_double_t*) Y;
  register complexe_double_t * dot = (complexe_double_t* ) dotc;
  complexe_double_t c_tmp ;
  double real = 0.0;
  double imm =0.0;

  #pragma omp parallel for reduction(+:real) reduction(+:imm)
  for (i=0; i < N ; i += incX)
    {
      complexe_double_t X_conjuge = {X2[i].real, -X2[i].imaginary} ;
      c_tmp = mult_complexe_double(X_conjuge, Y2[i]) ;
      real += c_tmp.real ;
      imm += c_tmp.imaginary;
    }
    dot->real = real ;
    dot->imaginary = imm;
  return ;
}
