#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"
#include <omp.h>


void mnblas_saxpy_open (const int N,const float alpha, const float *X,
        const int incX,  float *Y, const int incY )
  {
    register unsigned int i;

    #pragma omp parallel for
    for(i=0; i<N; i+=incX){
      Y[i] += alpha*X[i] ;
    }
  }


void mnblas_daxpy_open (const int N,const double alpha, const double *X,
        const int incX,  double *Y, const int incY )
  {
    register unsigned int i ;

    #pragma omp parallel for
    for(i=0; i<N; i+=incX){
      Y[i] += alpha*X[i] ;
    }
  }

void mnblas_caxpy_open (const int N,const void *alpha, const void *X,
        const int incX,  void *Y, const int incY )
  {
    register unsigned int i ;
    register complexe_float_t * X2 = (complexe_float_t*) X;
    register complexe_float_t * Y2 = (complexe_float_t*) Y;
    register complexe_float_t alphalaire = *((complexe_float_t*)alpha);

    #pragma omp parallel for
    for(i=0; i<N; i+=incX){
      Y2[i] = add_complexe_float(Y2[i] , mult_complexe_float(alphalaire, X2[i]));
    }

  }

void mnblas_zaxpy_open (const int N,const void *alpha, const void *X,
        const int incX,  void *Y, const int incY )
  {
    register unsigned int i ;
    register complexe_double_t * X2 = (complexe_double_t*) X;
    register complexe_double_t * Y2 = (complexe_double_t*) Y;
    register complexe_double_t alphalaire = *((complexe_double_t*)alpha);

    #pragma omp parallel for
    for(i=0; i<N; i+=incX){
      Y2[i] = add_complexe_double(Y2[i] , mult_complexe_double(alphalaire, X2[i]));
    }
  }
