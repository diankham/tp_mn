#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"


void mnblas_saxpy (const int N,const float alpha, const float *X,
        const int incX,  float *Y, const int incY )
  {
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;

    for(; i<N; i+=incX, j+=incY){
      Y[j] += alpha*X[i] ;
    }
  }


void mnblas_daxpy (const int N,const double alpha, const double *X,
        const int incX,  double *Y, const int incY )
  {
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;

    for(; i<N; i+=incX, j+=incY){
      Y[j] += alpha*X[i] ;
    }
  }

void mnblas_caxpy (const int N,const void *alpha, const void *X,
        const int incX,  void *Y, const int incY )
  {
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    register complexe_float_t * X2 = (complexe_float_t*) X;
    register complexe_float_t * Y2 = (complexe_float_t*) Y;
    register complexe_float_t alphalaire = *((complexe_float_t*)alpha);

    for(; i<N; i+=incX, j+=incY){
      Y2[j] = add_complexe_float(Y2[j] , mult_complexe_float(alphalaire, X2[i]));
    }

  }

void mnblas_zaxpy (const int N,const void *alpha, const void *X,
        const int incX,  void *Y, const int incY )
  {
    register unsigned int i = 0 ;
    register unsigned int j = 0 ;
    register complexe_double_t * X2 = (complexe_double_t*) X;
    register complexe_double_t * Y2 = (complexe_double_t*) Y;
    register complexe_double_t alphalaire = *((complexe_double_t*)alpha);

    for(; i<N; i+=incX, j+=incY){
      Y2[j] = add_complexe_double(Y2[j] , mult_complexe_double(alphalaire, X2[i]));
    }
  }
