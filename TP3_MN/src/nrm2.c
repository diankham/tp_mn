#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"
#include <math.h>


//euclidian norme = racine ( somme ( real² + imm² ) )

float  mnblas_snrm2(const int N, const float *X, const int incX)
{
  register unsigned int i = 0 ;
  register float somme = 0.0;

  for(; i<N; i+=incX){
    somme+= X[i] * X[i];
  }
  return sqrtf(somme);
}


double mnblas_dnrm2(const int N, const double *X, const int incX)
{
  register unsigned int i = 0 ;
  register double somme = 0.0;

  for(; i<N; i+=incX){
    somme+= X[i] * X[i];
  }
  return sqrt(somme);
}


float  mnblas_scnrm2(const int N, const void *X, const int incX)
{
  register unsigned int i = 0 ;
  register float somme = 0.0;
  register complexe_float_t * X2 = (complexe_float_t*) X;

  for(; i<N; i+=incX){
    somme+= X2[i].real * X2[i].real +  X2[i].imaginary * X2[i].imaginary;
  }
  return sqrtf(somme);

}


double mnblas_dznrm2(const int N, const void *X, const int incX)
{
  register unsigned int i = 0 ;
  register double somme = 0.0;
  register complexe_double_t * X2 = (complexe_double_t*) X;

  for(; i<N; i+=incX){
    somme+= X2[i].real * X2[i].real +  X2[i].imaginary * X2[i].imaginary;
  }
  return sqrt(somme);
  
}
