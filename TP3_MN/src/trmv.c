#include "mnblas.h"
#include <stdio.h>
#include "complexe.h"



void cblas_strmv(const MNCBLAS_LAYOUT Layout, const MNCBLAS_UPLO uplo, const MNCBLAS_TRANSPOSE trans, const MNCBLAS_DIAG diag, const int n, const float *a, const int lda, float *x, const int incx) {
    int i, j;
    float temp;

    if (trans == MNCblasNoTrans) {
        for (i = 0; i < n; i++) {
            temp = 0.0f;
            for (j = 0; j <= i; j++) {
                temp += a[i * lda + j] * x[j * incx];
            }
            x[i * incx] = temp;
        }
    } else if (trans == MNCblasTrans) {
        for (i = n - 1; i >= 0; i--) {
            temp = 0.0f;
            for (j = n - 1; j >= i; j--) {
                temp += a[i * lda + j] * x[j * incx];
            }
            x[i * incx] = temp;
        }
    } 
}

void cblas_dtrmv(const MNCBLAS_LAYOUT Layout, const MNCBLAS_UPLO uplo, const MNCBLAS_TRANSPOSE trans, const MNCBLAS_DIAG diag, const int n, const double *a, const int lda, double *x, const int incx) {
    int i, j;
    double temp;

    if (trans == MNCblasNoTrans) {
        for (i = 0; i < n; i++) {
            temp = 0.0;
            for (j = 0; j <= i; j++) {
                temp += a[i * lda + j] * x[j * incx];
            }
            x[i * incx] = temp;
        }
    } else if (trans == MNCblasTrans) {
        for (i = n - 1; i >= 0; i--) {
            temp = 0.0;
            for (j = n - 1; j >= i; j--) {
                temp += a[i * lda + j] * x[j * incx];
            }
            x[i * incx] = temp;
        }
    }
}

void cblas_ctrmv(const MNCBLAS_LAYOUT Layout, const MNCBLAS_UPLO uplo, const MNCBLAS_DIAG trans, const MNCBLAS_DIAG diag, const int n, const void *a, const int lda, void *x, const int incx) {
    int i, j;
    complexe_float_t temp;


    if (trans == MNCblasNoTrans) {
        for (i = 0; i < n; i++) {
            temp.real = 0.0f;
            temp.imaginary = 0.0f;
            for (j = 0; j <= i; j++) {
                temp = add_complexe_float(temp, mult_complexe_float(((complexe_float_t *)a)[i * lda + j], ((complexe_float_t *)x)[j * incx]));
            }
            ((complexe_float_t *)x)[i * incx] = temp;
        }
    } else if (trans == MNCblasTrans) {
        for (i = n - 1; i >= 0; i--) {
            temp.real = 0.0f;
            temp.imaginary = 0.0f;
            for (j = n - 1; j >= i; j--) {
                temp = add_complexe_float(temp, mult_complexe_float(((complexe_float_t *)a)[i * lda + j], ((complexe_float_t *)x)[j * incx]));
            }
            ((complexe_float_t *)x)[i * incx] = temp;
        }
    }
    else if (trans == MNCblasConjTrans) {
        for (i = n - 1; i >= 0; i--) {
            temp.real = 0.0f;
            temp.imaginary = 0.0f;
            for (j = n - 1; j >= i; j--) {
                temp = add_complexe_float(temp, mult_complexe_float(conj_complexe_float(((complexe_float_t *)a)[i * lda + j]), ((complexe_float_t *)x)[j * incx]));
            }
            ((complexe_float_t *)x)[i * incx] = temp;
        }
    } 
}

void cblas_ztrmv(const MNCBLAS_LAYOUT Layout, const MNCBLAS_UPLO uplo, const MNCBLAS_TRANSPOSE trans, const MNCBLAS_DIAG diag, const int n, const void *a, const int lda, void *x, const int incx) {
    int i, j;
    complexe_double_t temp;

    
    if (trans == MNCblasNoTrans) {
        for (i = 0; i < n; i++) {
            temp.real = 0.0;
            temp.imaginary = 0.0;
            for (j = 0; j <= i; j++) {
                temp = add_complexe_double(temp, mult_complexe_double(((complexe_double_t *)a)[i * lda + j], ((complexe_double_t *)x)[j * incx]));
            }
            ((complexe_double_t *)x)[i * incx] = temp;
        }
    } else if (trans == MNCblasTrans) {
        for (i = n - 1; i >= 0; i--) {
            temp.real = 0.0;
            temp.imaginary = 0.0;
            for (j = n - 1; j >= i; j--) {
                temp = add_complexe_double(temp, mult_complexe_double(((complexe_double_t *)a)[i * lda + j], ((complexe_double_t *)x)[j * incx]));
            }
            ((complexe_double_t *)x)[i * incx] = temp;
        }
    }
    else if (trans == MNCblasConjTrans) {
        for (i = n - 1; i >= 0; i--) {
            temp.real = 0.0;
            temp.imaginary = 0.0;
            for (j = n - 1; j >= i; j--) {
                temp = add_complexe_double(temp, mult_complexe_double(conj_complexe_double(((complexe_double_t *)a)[i * lda + j]), ((complexe_double_t *)x)[j * incx]));
            }
            ((complexe_double_t *)x)[i * incx] = temp;
        } 
    }
}
