#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"




void mncblas_sgemv(const MNCBLAS_LAYOUT layout,//Y = alpha*a * X + b*y
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY)
{
  register unsigned int i,j;

  for (i=0;i<N;i+=incX){
    register float tmp = 0.0;
    for(j=0;j<N;j+=incY){
      tmp += A[i*N+j]*X[j];
    }
    Y[i] = Y[i] * beta + tmp * alpha;
  }
}



void mncblas_dgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY)
{
  register unsigned int i,j;

  for (i=0;i<N;i+=incX){
    register double tmp = 0.0;
    for(j=0;j<N;j+=incY){
      tmp += A[i*N+j]*X[j];
    }
    Y[i] = Y[i] * beta + tmp * alpha;
  }
}


void mncblas_cgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY)
{
  register unsigned int i,j;
  register complexe_float_t * A2 = (complexe_float_t* ) A;
  register complexe_float_t * X2 = (complexe_float_t*) X;
  register complexe_float_t * Y2 = (complexe_float_t*) Y;
  register complexe_float_t alpha2 = *((complexe_float_t*) alpha);
  register complexe_float_t beta2 = *((complexe_float_t*) beta);

  for (i=0;i<N;i+=incX){
    register complexe_float_t tmp = (complexe_float_t){0.0,0.0};
    for(j=0;j<N;j+=incY){
      tmp = add_complexe_float(tmp, mult_complexe_float(A2[i*N+j], X2[j]));
    }
    Y2[i] = add_complexe_float(mult_complexe_float(Y2[i], beta2) , mult_complexe_float(tmp, alpha2));
  }
}


void mncblas_zgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY)
{
  register unsigned int i,j;
  register complexe_double_t * A2 = (complexe_double_t* ) A;
  register complexe_double_t * X2 = (complexe_double_t*) X;
  register complexe_double_t * Y2 = (complexe_double_t*) Y;
  register complexe_double_t alpha2 = *((complexe_double_t*) alpha);
  register complexe_double_t beta2 = *((complexe_double_t*) beta);

  for (i=0;i<N;i+=incX){
    register complexe_double_t tmp = (complexe_double_t){0.0,0.0};
    for(j=0;j<N;j+=incY){
      tmp = add_complexe_double(tmp, mult_complexe_double(A2[i*N+j], X2[j]));
    }
    Y2[i] = add_complexe_double(mult_complexe_double(Y2[i], beta2) , mult_complexe_double(tmp, alpha2));
  }
}
