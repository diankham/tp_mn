#include "mnblas.h"
#include "complexe2.h"
#include <omp.h>

void mncblas_scopy_open(const int N, const float *X, const int incX,
                 float *Y, const int incY)
{
  register unsigned int i = 0 ;

  #pragma omp parallel for
  for (i=0; i < N ; i += incX)
    {
      Y [i] = X [i] ;
    }
  return ;
}

void mncblas_dcopy_open(const int N, const double *X, const int incX,
                 double *Y, const int incY)
{
  register unsigned int i = 0 ;

  #pragma omp parallel for
  for (i=0; i < N ; i += incX)
    {
      Y [i] = X [i] ;
    }
  return ;
}

void mncblas_ccopy_open(const int N, const void *X, const int incX,
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;

  complexe_float_t * X2 = (complexe_float_t*) X;
  complexe_float_t * Y2 = (complexe_float_t*) Y;

  #pragma omp parallel for
  for (i=0; i < N ; i += incX)
    {
      Y2 [i] =  X2 [i] ;
    }
  return ;
}

void mncblas_zcopy_open(const int N, const void *X, const int incX,
		                    void *Y, const int incY)
{
  register unsigned int i = 0 ;

  complexe_double_t * X2 = (complexe_double_t*) X;
  complexe_double_t * Y2 = (complexe_double_t*) Y;

  #pragma omp parallel for
  for (i=0; i < N ; i += incX)
    {
      Y2 [i] =  X2 [i] ;
    }
  return ;
}
