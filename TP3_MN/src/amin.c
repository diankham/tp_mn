#include "mnblas.h"
#include <stdio.h>
#include "complexe2.h"


CBLAS_INDEX mnblas_isamin(const int N, const float  *X, const int incX)
  {
    if(N == 0 || incX == 0){
      return 0;
    }else{
      register CBLAS_INDEX min_index =0;
      register float min = abs_float(X[0]);
      register unsigned int i = incX ;

      for(; i<N; i+=incX){
        float val_X = abs_float(X[i]) ;
        if(val_X < min){
          min_index = i ;
          min = val_X;
        }
      }
      return min_index;
    }
  }


CBLAS_INDEX mnblas_idamin(const int N, const double *X, const int incX)
  {
    if(N == 0 || incX == 0){
      return 0;
    }else{
      register CBLAS_INDEX min_index =0;
      register double min = abs_double(X[0]);
      register unsigned int i = incX ;

      for(; i<N; i+=incX){
        double val_X = abs_double(X[i]) ;
        if(val_X < min){
          min_index = i ;
          min = val_X;
        }
      }
      return min_index;
    }
  }


CBLAS_INDEX mnblas_icamin(const int N, const void   *X, const int incX)
  {
    if(N == 0 || incX == 0){
      return 0;
    }else{
      register CBLAS_INDEX min_index =0;
      register complexe_float_t * X2 = (complexe_float_t*) X;
      register float min = abs_float(X2[min_index].imaginary) + abs_float(X2[min_index].real) ;
      register unsigned int i = incX ;

      for(; i<N; i+=incX){
        float val_X = abs_float(X2[i].imaginary) + abs_float(X2[i].real);
        if(val_X < min){
          min_index = i ;
          min = val_X;
        }
      }
      return min_index;
    }
  }


CBLAS_INDEX mnblas_izamin(const int N, const void   *X, const int incX)
  {
    if(N == 0 || incX == 0){
      return 0;
    }else{
      register CBLAS_INDEX min_index =0;
      register complexe_double_t * X2 = (complexe_double_t*) X;
      register float min = abs_float(X2[min_index].imaginary) + abs_float(X2[min_index].real) ;
      register unsigned int i = incX ;

      for(; i<N; i+=incX){
        double val_X = abs_double(X2[i].imaginary) + abs_double(X2[i].real);
        if(val_X < min){
          min_index = i ;
          min = val_X;
        }
      }
      return min_index;
    }
  }
