#include "test_vect.h"

int main(int argc, char const *argv[]) {
  presente_var_global();
  separator_test("test_complexe1");
  test_complexe1();
  separator_test("test_complexe2");
  test_complexe2();
  separator_test("test_complexe3");
  test_complexe3();
  separator_test("test_complexe4");
  test_complexe4();
  separator_test("test_copy");
  test_copy();
  separator_test("test_copy OPENopen");
  test_copy_open();
  separator_test("test_swap");
  test_swap();
  separator_test("test_dot");
  test_dot();
  separator_test("test_dot OPENopen");
  test_dot_open ();
  separator_test("test_axpy");
  test_axpy();
  separator_test("test_axpy OPENopen");
  test_axpy_open ();
  separator_test("test_asum");
  test_asum();
  separator_test("test_amax");
  test_amax();
  separator_test("test_amin");
  test_amin();
  separator_test("test_nrm2");
  test_nrm2();
  separator_test("test_gemv");
  test_gemv();
  separator_test("test_gemv OPENopen");
  test_gemv_open ();
  separator_test("test_gemm");
  test_gemm();
  separator_test("test_gemm OPENopen");
  test_gemm_open ();
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

void presente_var_global(){
  printf(" NB_OPERATION = %d \n VECSIZE = %d \n NB_FOIS_VEC = %d \n NB_FOIS_NORMAL = %d \n" , NB_OPERATION, VECSIZE, NB_FOIS_VEC, NB_FOIS_NORMAL);
}
