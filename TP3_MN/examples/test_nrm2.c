#include "test_vect.h"
#include <math.h>

void test_performance_complexe_double_nrm2();
void test_performance_complexe_float_nrm2();
void test_performance_double_nrm2();
void test_performance_float_nrm2();
void test_assert_nrm2();



void test_nrm2 ()
{
  test_assert_nrm2();
  test_performance_complexe_double_nrm2();
  test_performance_complexe_float_nrm2();
  test_performance_double_nrm2();
  test_performance_float_nrm2();
}


void test_performance_complexe_double_nrm2(){
  printf("performance complexe double : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
  vcom_douple vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_double (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_dznrm2 (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*4);
}

void test_performance_complexe_float_nrm2(){
  printf("performance complexe float : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_float_t b  = (complexe_float_t) {25.0, 32.0} ;
  vcom_float vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_float (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_scnrm2 (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*4);
}

void test_performance_double_nrm2(){
  printf("performance  double : \n");
  struct timespec start_timespec, end_timespec ;

  register double b  = 25.0 ;
  vdouble vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_double (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_dnrm2 (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*2);
}

void test_performance_float_nrm2(){
  printf("performance float : \n");
  struct timespec start_timespec, end_timespec ;

  register float b  = 25.0 ;
  vfloat vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_float (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_snrm2 (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*2);
}

void test_assert_nrm2(){

  vfloat vec1;
  vector_init_float (vec1, 0.0) ;
  for(int j = 0; j < 10 ;j++){
    vec1[j]=5;
  }
  float res_float = mnblas_snrm2 (VECSIZE, vec1,1) ;
  assert(res_float == sqrtf(10*25));


  complexe_double_t a  = (complexe_double_t) {0.0, 0.0} ;
  complexe_double_t b = (complexe_double_t) {5.0, 5.0} ;

  vcom_douple v1;
  vector_init_com_double(v1,a);
  for(int j = 0; j<10;j++){
    v1[j] = b ;
  }
  double res_double = mnblas_dznrm2(VECSIZE,v1,1);
  assert(res_double == sqrt(10*50));


  printf("test réussi\n\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_nrm2");
  test_nrm2();
}