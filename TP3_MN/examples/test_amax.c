#include "test_vect.h"

void test_performance_complexe_double_amax();
void test_performance_complexe_float_amax();
void test_performance_double_amax();
void test_performance_float_amax();
void test_assert_amax();



void test_amax ()
{
  test_assert_amax();
  test_performance_complexe_double_amax();
  test_performance_complexe_float_amax();
  test_performance_double_amax();
  test_performance_float_amax();
}


void test_performance_complexe_double_amax(){
  printf("performance complexe double : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
  vcom_douple vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_double (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_izamax (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*2);
}

void test_performance_complexe_float_amax(){
  printf("performance complexe float : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_float_t b  = (complexe_float_t) {25.0, 32.0} ;
  vcom_float vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_float (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_icamax (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*2);
}

void test_performance_double_amax(){
  printf("performance double : \n");
  struct timespec start_timespec, end_timespec ;

  register double b  = 25.0 ;
  vdouble vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_double (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_idamax (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*1);
}
void test_performance_float_amax(){
  printf("performance float : \n");
  struct timespec start_timespec, end_timespec ;

  register float b  = 25.0 ;
  vfloat vec1;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_float (vec1, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_isamax (VECSIZE, vec1,1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*1);
}

void test_assert_amax(){

  vfloat vec1;
  vector_init_float (vec1, 0.0) ;
  for(int j = 0; j < 10 ;j++){
    vec1[j]=5.0;
  }
  vec1[15] = 10.0;
  int pos = mnblas_isamax (VECSIZE, vec1,1) ;
  assert(pos == 15);


  complexe_double_t a  = (complexe_double_t) {0.0, 0.0} ;
  complexe_double_t b = (complexe_double_t) {5.0, 5.0} ;

  vcom_douple v1;
  vector_init_com_double(v1,a);
  for(int j = 0; j<10;j++){
    v1[j] = b ;
  }
  v1[52] = (complexe_double_t) {-10.0, 10.0} ;
  pos = mnblas_izamax(VECSIZE,v1,1);
  assert(pos == 52);

  printf("test réussi\n\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_amax");
  test_amax();
}