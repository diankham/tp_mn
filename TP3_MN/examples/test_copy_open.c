#include "test_vect.h"

void test_performance_complexe_double_copy_open();
void test_performance_complexe_float_copy_open();
void test_performance_double_copy_open();
void test_performance_float_copy_open();
void test_assert_copy_open();


void test_copy_open ()
{
  test_assert_copy_open();
  test_performance_complexe_double_copy_open();
  test_performance_complexe_float_copy_open();
  test_performance_double_copy_open();
  test_performance_float_copy_open();
}


void test_performance_complexe_double_copy_open(){
  printf("\nperformance complexe double : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_double_t a  = (complexe_double_t) {10.0, 7.0} ;
  register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
  vcom_douple vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_double (vec1, a) ;
     vector_init_com_double (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_zcopy_open (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*16);
}

void test_performance_complexe_float_copy_open(){
  printf("\nperformance complexe float : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_float_t a  = (complexe_float_t) {10.0, 7.0} ;
  register complexe_float_t b  = (complexe_float_t) {25.0, 32.0} ;
  vcom_float vec1, vec2 ;


  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_float (vec1, a) ;
     vector_init_com_float (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_ccopy_open (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC, total_temp_timespec , NB_FOIS_VEC*VECSIZE*8);
}

void test_performance_double_copy_open(){
  printf("\nperformance double : \n");
  struct timespec start_timespec, end_timespec ;

  register double a  = 10.0 ;
  register double b  = 5.5 ;
  vdouble vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_double (vec1, a) ;
     vector_init_double (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_dcopy_open (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*8);
}

void test_performance_float_copy_open(){
  printf("\nperformance float : \n");
  struct timespec start_timespec, end_timespec ;

  register float a  = 10.0 ;
  register float b  = 5.5 ;
  vfloat vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_float (vec1, a) ;
     vector_init_float (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_scopy_open (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*4);
}

void test_assert_copy_open(){
  vfloat vec1, vec2 ;
  vector_init_float (vec1, 1.0) ;
  vector_init_float (vec2, 2.0) ;
  vec1[50] = 5.0;
  vec1[100] = 10.0;
  vec1[964] = 56.884;
  mncblas_scopy_open (VECSIZE, vec1, 1, vec2, 1) ;
  for(int i = 0 ; i < VECSIZE;i++){
    assert(vec1[i] == vec2[i]);
  }
  printf("test réussi\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_copy_open");
  test_copy_open();
}