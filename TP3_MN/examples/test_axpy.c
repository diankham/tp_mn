#include "test_vect.h"

void test_performance_complexe_double_axpy();
void test_performance_complexe_float_axpy();
void test_performance_double_axpy();
void test_performance_float_axpy();
void test_assert_axpy();



void test_axpy ()
{
  test_assert_axpy();
  test_performance_complexe_double_axpy();
  test_performance_complexe_float_axpy();
  test_performance_double_axpy();
  test_performance_float_axpy();
}


void test_performance_complexe_double_axpy(){
  printf("performance complexe double : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_double_t a  = (complexe_double_t) {10.0, 7.0} ;
  register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
  complexe_double_t c  = (complexe_double_t) {68.0, 5.0} ;
  vcom_douple vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_double (vec1, a) ;
     vector_init_com_double (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_zaxpy (VECSIZE, &c, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*8); // 8 car add_com -> 2op  et mult_comp -> 6op
}

void test_performance_complexe_float_axpy(){
  printf("performance complexe float : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_float_t a  = (complexe_float_t) {10.0, 7.0} ;
  register complexe_float_t b  = (complexe_float_t) {25.0, 32.0} ;
  complexe_float_t c  = (complexe_float_t) {68.0, 5.0} ;
  vcom_float vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_float (vec1, a) ;
     vector_init_com_float (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_caxpy (VECSIZE, &c, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*8); // 8 car add_com -> 2op  et mult_comp -> 6op
}


void test_performance_double_axpy(){
  printf("performance double : \n");
  struct timespec start_timespec, end_timespec ;

  register double a = 10.0;
  register double b = 32.5;
  double c = 0.0;
  vdouble vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_double (vec1, a) ;
     vector_init_double (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_daxpy (VECSIZE, c, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*2);
}


void test_performance_float_axpy(){
  printf("performance float : \n");
  struct timespec start_timespec, end_timespec ;

  register float a = 10.0;
  register float b = 32.5;
  float c = 0.0; 
  vfloat vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_float (vec1, a) ;
     vector_init_float (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mnblas_saxpy (VECSIZE, c, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , VECSIZE*NB_FOIS_VEC*2);
}


void test_assert_axpy(){

  vfloat vec1, vec2, vec3 ;
  vector_init_float (vec1, 0.0) ;
  vector_init_float (vec2, 0.0) ;
  for(int j = 0; j < 10 ;j++){
    vec1[j]=5; vec2[j] = 7;
  }
  mncblas_scopy(VECSIZE,vec2,1,vec3,1);
  float f = 5.0;
  mnblas_saxpy (VECSIZE, f, vec1, 1, vec3, 1) ;
  for(int i = 0 ; i < VECSIZE;i++){
    assert(vec3[i] == vec2[i] + vec1[i]*f);
  }


  complexe_double_t a  = (complexe_double_t) {0.0, 0.0} ;
  complexe_double_t b  = (complexe_double_t) {2.0, 2.0} ;
  complexe_double_t c = (complexe_double_t) {5.0, 5.0} ;

  vcom_douple v1,v2,v3;
  vector_init_com_double(v1,a);
  vector_init_com_double(v2,a);
  for(int j = 0; j<10;j++){
    v1[j] = b ; v2[j] = b;
  }
  mncblas_zcopy(VECSIZE,v2,1,v3,1);
  mnblas_zaxpy (VECSIZE, &c, v1, 1, v3, 1) ;
  for(int i = 0 ; i < 10;i++){
    assert(v3[i].real == 2.0  && v3[i].imaginary == 22.0 );
  }
  for(int i = 10 ; i < VECSIZE;i++){
    assert(v3[i].real == a.real && v3[i].imaginary == a.imaginary);
  }

  printf("test réussi\n\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_axpy");
  test_axpy();
}