#include "test_vect.h"

void test_performance_complexe_double_dot();
void test_performance_complexe_float_dot();
void test_performance_double_dot();
void test_performance_float_dot();
void test_assert_dot();



void test_dot ()
{
  test_assert_dot();
  test_performance_complexe_double_dot();
  test_performance_complexe_float_dot();
  test_performance_double_dot();
  test_performance_float_dot();
}


void test_performance_complexe_double_dot(){
  printf("performance complexe double : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_double_t a  = (complexe_double_t) {10.0, 7.0} ;
  register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
  vcom_douple vec1, vec2 ;
  complexe_double_t c = (complexe_double_t) {0.0, 0.0};
  complexe_double_t d ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_double (vec1, a) ;
     vector_init_com_double (vec2, b) ;
     d = c;
     TOP_NANO(start_timespec) ;
     mncblas_zdotu_sub (VECSIZE, vec1, 1, vec2, 1,&d) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*8);      //cout (dot)= VECSIZE * (cout (add) + cout (mult) = VECSIZE * (2+6)
}


void test_performance_complexe_float_dot(){
  printf("performance complexe float : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_float_t a  = (complexe_float_t) {10.0, 7.0} ;
  register complexe_float_t b  = (complexe_float_t) {25.0, 32.0} ;
  vcom_float vec1, vec2 ;
  complexe_float_t c = (complexe_float_t) {0.0, 0.0};
  complexe_float_t d ;


  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_float (vec1, a) ;
     vector_init_com_float (vec2, b) ;
     d = c;
     TOP_NANO(start_timespec) ;
     mncblas_cdotu_sub (VECSIZE, vec1, 1, vec2, 1,&d) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*8);      //cout (dot)= VECSIZE * (cout (add) + cout (mult) = VECSIZE * (2+6)
}

void test_performance_double_dot(){
  printf("performance double : \n");
  struct timespec start_timespec, end_timespec ;

  register double a  = 10.0 ;
  register double b  = 32.5 ;
  vdouble vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_double (vec1, a) ;
     vector_init_double (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_ddot (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*2);      //cout (dot)= VECSIZE * (cout (add) + cout (mult) = VECSIZE * (2+6)
}

void test_performance_float_dot(){
  printf("performance float : \n");
  struct timespec start_timespec, end_timespec ;

  register float a  = 10.0 ;
  register float b  = 32.5 ;
  vfloat vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_float (vec1, a) ;
     vector_init_float (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_sdot (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*2);      //cout (dot)= VECSIZE * (cout (add) + cout (mult) = VECSIZE * (2+6)
}


void test_assert_dot(){
  vfloat vec1, vec2 ;
  vector_init_float (vec1, 0.0) ;
  vector_init_float (vec2, 0.0) ;
  for(int j = 0; j < 10 ;j++){
    vec1[j]=5; vec2[j] = 7;
  }
  float f =mncblas_sdot (VECSIZE, vec1, 1, vec2, 1) ;
  assert(f == 5*7*10);

  complexe_double_t a  = (complexe_double_t) {0.0, 0.0} ;
  complexe_double_t b  = (complexe_double_t) {2.0, 2.0} ;

  vcom_douple v1,v2;
  vector_init_com_double(v1,a);
  vector_init_com_double(v2,a);
  for(int j = 0; j<10;j++){
    v1[j] = b ; v2[j] = b;
  }
  mncblas_zdotu_sub(VECSIZE,v1,1,v2,1,&a);
  assert(a.real == 0.0 && a.imaginary == 8.0*10);

  printf("test réussi\n\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_dot");
  test_dot();
}