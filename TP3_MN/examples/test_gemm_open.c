#include "test_vect.h"

void test_performance_complexe_double_gemm_open();
void test_performance_complexe_float_gemm_open();
void test_performance_double_gemm_open();
void test_performance_float_gemm_open();
void test_assert_gemm_open();



void test_gemm_open ()
{
  test_assert_gemm_open();
  test_performance_complexe_double_gemm_open();
  test_performance_complexe_float_gemm_open();
  test_performance_double_gemm_open();
  test_performance_float_gemm_open();
}

void test_performance_complexe_double_gemm_open(){
  printf("performance complexe double : \n");
  struct timespec start_timespec, end_timespec;

  register complexe_double_t b  = (complexe_double_t) {2.0, 2.0} ;
  complexe_double_t c  = (complexe_double_t) {4.0, 4.0} ;
  complexe_double_t alpha_c = (complexe_double_t){2.0,2.0};
  mat_com_douple C, A , B;


  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for(int i = 0; i<MATRICE_SIZE*MATRICE_SIZE;i++){
       A[i] = c; B[i]=c;C[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_zgemm_open(101,111,111,MATRICE_SIZE,MATRICE_SIZE,MATRICE_SIZE,&alpha_c,A,0,B,0,&alpha_c,C,0); // alpha = 2.0 , beta =
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (unsigned long long int)(MATRICE_SIZE*MATRICE_SIZE*MATRICE_SIZE*(6+6+2) + MATRICE_SIZE*MATRICE_SIZE*6);
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}


void test_performance_complexe_float_gemm_open(){
  printf("performance complexe float : \n");
  struct timespec start_timespec, end_timespec;

  register complexe_float_t b  = (complexe_float_t) {2.0, 2.0} ;
  complexe_float_t c  = (complexe_float_t) {4.0, 4.0} ;
  complexe_float_t alpha_c = (complexe_float_t){2.0,2.0};
  mat_com_float C, A , B;


  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for(int i = 0; i<MATRICE_SIZE*MATRICE_SIZE;i++){
       A[i] = c; B[i]=c;C[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_cgemm_open(101,111,111,MATRICE_SIZE,MATRICE_SIZE,MATRICE_SIZE,&alpha_c,A,0,B,0,&alpha_c,C,0); // alpha = 2.0 , beta =
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (unsigned long long int)(MATRICE_SIZE*MATRICE_SIZE*MATRICE_SIZE*(6+6+2) + MATRICE_SIZE*MATRICE_SIZE*6);
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}

void test_performance_double_gemm_open(){
  printf("performance double : \n");
  struct timespec start_timespec, end_timespec;

  register double b  = 2.0 ;
  double c  = 4.0 ;
  double alpha_c = 2.0 ;
  mat_double C, A , B;


  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for(int i = 0; i<MATRICE_SIZE*MATRICE_SIZE;i++){
       A[i] = c; B[i]=c;C[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_dgemm_open(101,111,111,MATRICE_SIZE,MATRICE_SIZE,MATRICE_SIZE,alpha_c,A,0,B,0,alpha_c,C,0); // alpha = 2.0 , beta =
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (unsigned long long int)(MATRICE_SIZE*MATRICE_SIZE*MATRICE_SIZE*3 + MATRICE_SIZE*MATRICE_SIZE);
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}

void test_performance_float_gemm_open(){
  printf("performance float : \n");
  struct timespec start_timespec, end_timespec;

  register float b  = 2.0 ;
  float c  = 4.0 ;
  float alpha_c = 2.0 ;
  mat_float C, A , B;


  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for(int i = 0; i<MATRICE_SIZE*MATRICE_SIZE;i++){
       A[i] = c; B[i]=c;C[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_sgemm_open(101,111,111,MATRICE_SIZE,MATRICE_SIZE,MATRICE_SIZE,alpha_c,A,0,B,0,alpha_c,C,0); // alpha = 2.0 , beta =
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (unsigned long long int)(MATRICE_SIZE*MATRICE_SIZE*MATRICE_SIZE*3 + MATRICE_SIZE*MATRICE_SIZE);
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}


void test_assert_gemm_open(){
  float matrice_f_A[9],matrice_f_B[9],matrice_f_C[9];
  for(int i = 0; i<9;i++){
    matrice_f_A[i] = 4.0; matrice_f_B[i]=4.0;
  }
  for(int i = 0; i<9;i++){
    matrice_f_C[i] = 2.0;
  }
  int taille = 3;
  float alpha = 2.0;
  float beta = 2.0;
  mncblas_sgemm_open(101,111,111,taille,taille,taille,alpha,matrice_f_A,0,matrice_f_B,0,beta,matrice_f_C,0); // alpha = 2.0 , beta =
  //matr_A_B_alpha [0] = somme (i =0..2) (A[i] * B[N*i]) * alpha = 4*4*3*2 = 96
  // C[0]= matr_A_B_alpha[0]+ C[0]*beta = 96 + 2*2 = 100
  for(int i = 0; i<9;i++){
    assert(matrice_f_C[i]==100.0);
  }



  register complexe_double_t b  = (complexe_double_t) {2.0, 2.0} ;
  complexe_double_t c  = (complexe_double_t) {4.0, 4.0} ;
  complexe_double_t alpha_c = (complexe_double_t){2.0,2.0};
  complexe_double_t C[9], A[9], B[9];

  for(int i = 0; i<9;i++){
    A[i] = c; B[i]=c;
  }
  for(int i = 0; i<9;i++){
    C[i] = b;
  }

  mncblas_zgemm_open(101,111,111,taille,taille,taille,&alpha_c,A,0,B,0,&alpha_c,C,0); // alpha = 2.0 , beta =
  // matr_A_B_alpha = somme (i =0..2) (A[i] * B[N*i]) * alpha = (4.0 + 4.0*i)²*3 * alpha = 96*i * (2.0 +2.0*i) = -192 + 192*i
  // C[0] = matr_A_B_alpha[0] + beta * C[0] = (-192+192*i) + (2.0 + 2.0*i)(2.0 +2.0*i) = -192 + 200*i
  for(int i = 0; i<9;i++){
    assert(C[i].real==-192.0 && C[i].imaginary == 200.0);
  }
  printf("test réussi\n\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_gemm_open");
  test_gemm_open();
}