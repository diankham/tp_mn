#include "test_vect.h"

void test_performance_complexe_double_gemv_open();
void test_performance_complexe_float_gemv_open();
void test_performance_double_gemv_open();
void test_performance_float_gemv_open();
void test_assert_gemv_open();



void test_gemv_open ()
{
  test_assert_gemv_open();
  test_performance_complexe_double_gemv_open();
  test_performance_complexe_float_gemv_open();
  test_performance_double_gemv_open();
  test_performance_float_gemv_open();
}


void test_performance_complexe_double_gemv_open(){
  printf("performance complexe double : \n");
  struct timespec start_timespec, end_timespec;
  mat_com_douple matrice ;
  register complexe_double_t a  = (complexe_double_t) {0.0, 0.0} ;
  register complexe_double_t b  = (complexe_double_t) {2.0, 2.0} ;
  complexe_double_t c  = (complexe_double_t) {4.0, 4.0} ;
  matrice_init_com_double(matrice , a);

  complexe_double_t X[MATRICE_SIZE],Y[MATRICE_SIZE];

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for (int i = 0; i < MATRICE_SIZE; i++){
       X[i] = b; Y[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_zgemv_open(101,111,MATRICE_SIZE,MATRICE_SIZE,&c,matrice,0,X,1,&c,Y,1);
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (MATRICE_SIZE*MATRICE_SIZE*(6+2) + MATRICE_SIZE*(6+6+2));
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}

void test_performance_complexe_float_gemv_open(){
  printf("performance complexe float : \n");
  struct timespec start_timespec, end_timespec;
  mat_com_float matrice ;
  register complexe_float_t a  = (complexe_float_t) {0.0, 0.0} ;
  register complexe_float_t b  = (complexe_float_t) {2.0, 2.0} ;
  complexe_float_t c  = (complexe_float_t) {4.0, 4.0} ;
  matrice_init_com_float(matrice , a);

  complexe_float_t X[MATRICE_SIZE],Y[MATRICE_SIZE];

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for (int i = 0; i < MATRICE_SIZE; i++){
       X[i] = b; Y[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_cgemv_open(101,111,MATRICE_SIZE,MATRICE_SIZE,&c,matrice,0,X,1,&c,Y,1);
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (MATRICE_SIZE*MATRICE_SIZE*(6+2) + MATRICE_SIZE*(6+6+2));
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}

void test_performance_double_gemv_open(){
  printf("performance double : \n");
  struct timespec start_timespec, end_timespec;
  mat_double matrice ;
  register double a  = 0.0 ;
  register double b  = 2.0 ;
  double c  = 4.0 ;
  matrice_init_double(matrice , a);

  double X[MATRICE_SIZE],Y[MATRICE_SIZE];

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for (int i = 0; i < MATRICE_SIZE; i++){
       X[i] = b; Y[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_dgemv_open(101,111,MATRICE_SIZE,MATRICE_SIZE,c,matrice,0,X,1,c,Y,1);
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (MATRICE_SIZE*MATRICE_SIZE*(2) + MATRICE_SIZE*(3));
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}

void test_performance_float_gemv_open(){
  printf("performance float : \n");
  struct timespec start_timespec, end_timespec;
  mat_float matrice ;
  register float a  = 0.0 ;
  register float b  = 2.0 ;
  float c  = 4.0 ;
  matrice_init_float(matrice , a);

  float X[MATRICE_SIZE],Y[MATRICE_SIZE];

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     for (int i = 0; i < MATRICE_SIZE; i++){
       X[i] = b; Y[i] = b;
     }
     TOP_NANO(start_timespec) ;
     mncblas_sgemv_open(101,111,MATRICE_SIZE,MATRICE_SIZE,c,matrice,0,X,1,c,Y,1);
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   unsigned long long int nb_op = NB_FOIS_VEC* (MATRICE_SIZE*MATRICE_SIZE*(2) + MATRICE_SIZE*(3));
   affiche_perf(NB_FOIS_VEC , total_temp_timespec , nb_op);
}

void test_assert_gemv_open(){
  register complexe_double_t b  = (complexe_double_t) {2.0, 2.0} ;
  complexe_double_t c  = (complexe_double_t) {4.0, 4.0} ;
  complexe_double_t matrice[9];
  complexe_double_t X[3];
  complexe_double_t Y[3];

  float vec1[3],vec2[3],matrice_float[9];
  for(int i = 0; i<3;i++){
    vec1[i] = 4.0; vec2[i]=4.0;
  }
  for(int i = 0; i<9;i++){
    matrice_float[i] = 2.0;
  }
  mncblas_sgemv_open(101,111,3,3,2.0,matrice_float,0,vec1,1,2.0,vec2,1);
  // x*A*alpha[1] = somme(X[0..2] * A[0..2]) * 2.0 = 4*2*3*2 = 48
  // y = 48 + 4*2 = 56
  for(int i = 0; i<3;i++){
    assert(vec2[i]==56.0);
  }


  for(int i =0; i<9; i++){
    matrice[i]= b;
  }
  for(int i =0; i<3; i++){
    X[i]= c; Y[i]= c;
  }
  mncblas_zgemv_open(101,111,3,3,&c,matrice,0,X,1,&c,Y,1);
  // x*a*alpha = somme (x[0..2] * a[0..2]) * 4.0 +i*4.0 = (2.0 +i*2.0)*(4.0+ i*4.0)*3 * (4.0 + i*4.0) = -192 + 192*i
  // y = (-192+192*i) + (4.0 + 4.0*i)(4.0 +4.0*i) = -192 + 224*i
  for(int i = 0; i<3;i++){
    assert(Y[i].real==-192.0 && Y[i].imaginary == 224.0);
  }
  printf("test réussi\n\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_gemv_open");
  test_gemv_open();
}