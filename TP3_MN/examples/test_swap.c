#include "test_vect.h"

void test_performance_complexe_double_swap();
void test_performance_double_swap();
void test_performance_complexe_float_swap();
void test_performance_float_swap();
void test_assert_swap();



void test_swap ()
{
  test_assert_swap();
  test_performance_complexe_double_swap();
  test_performance_double_swap();
  test_performance_complexe_float_swap();
  test_performance_float_swap();
}


void test_performance_complexe_double_swap(){
  printf("performance complexe double : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_double_t a  = (complexe_double_t) {10.0, 7.0} ;
  register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
  vcom_douple vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_double (vec1, a) ;
     vector_init_com_double (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_zswap (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*3*16); // 3 opération de copie * taille structure
}

void test_performance_double_swap(){
  printf("performance double : \n");
  struct timespec start_timespec, end_timespec ;

  register double a  = 10.0 ;
  register double b  = 25.0 ;
  vdouble vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_double (vec1, a) ;
     vector_init_double (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_dswap (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*3*8);
}

void test_performance_complexe_float_swap(){
  printf("performance complexe float : \n");
  struct timespec start_timespec, end_timespec ;

  register complexe_float_t a  = (complexe_float_t) {10.0, 7.0} ;
  register complexe_float_t b  = (complexe_float_t) {25.0, 32.0} ;
  vcom_float vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_com_float (vec1, a) ;
     vector_init_com_float (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_cswap (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*3*8);
}
void test_performance_float_swap(){
  printf("performance float : \n");
  struct timespec start_timespec, end_timespec ;

  register float a  = 10.0 ;
  register float b  = 25.0 ;
  vfloat vec1, vec2 ;

  double total_temp_timespec = 0.0;
   for(int i = 0; i< NB_FOIS_VEC;i++){
     vector_init_float (vec1, a) ;
     vector_init_float (vec2, b) ;
     TOP_NANO(start_timespec) ;
     mncblas_sswap (VECSIZE, vec1, 1, vec2, 1) ;
     TOP_NANO(end_timespec) ;
     total_temp_timespec += tdiff_nano(&start_timespec,&end_timespec);
   }
   affiche_res_double(NB_FOIS_VEC , total_temp_timespec , NB_FOIS_VEC*VECSIZE*3*4);
}



void test_assert_swap(){
  vfloat vec1, vec2 ;
  vector_init_float (vec1, 1.0) ;
  vector_init_float (vec2, 2.0) ;
  vec1[50] = 5.0; vec2[50] = 8.0;
  vec1[100] = 10.0; vec2[100] =  -3.0;
  vec1[964] = 56.884; vec2[964] = 87.884;

  vfloat v1, v2;
  mncblas_scopy(VECSIZE,vec1,1,v1,1);
  mncblas_scopy(VECSIZE,vec2,1,v2,1);
  mncblas_sswap (VECSIZE, vec1, 1, vec2, 1) ;
  for(int i = 0 ; i < VECSIZE;i++){
    assert(vec1[i] == v2[i]);
    assert(vec2[i] == v1[i]);
  }

  printf("test réussi\n\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_swap");
  test_swap();
}

