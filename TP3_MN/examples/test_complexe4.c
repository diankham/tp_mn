#include "tests.h"
#include "complexe2.h"

void test_complexe4 ()
{
 complexe_double_t cd1 ;
 complexe_double_t cd2 ;
 complexe_double_t cd3 ;
 register complexe_double_t a  = (complexe_double_t) {10.0, 7.0} ;
 register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
 cd1=a;
 cd2=b;

 cd3 =add_complexe_double(cd1,cd2);
 assert(cd3.real == cd1.real + cd2.real);
 assert(cd3.imaginary == cd1.imaginary + cd2.imaginary);

 cd3 = mult_complexe_double(cd1, cd2);
 assert(cd3.real == cd1.real * cd2.real - cd1.imaginary * cd2.imaginary);
 assert(cd3.imaginary == cd1.real * cd2.imaginary + cd1.imaginary * cd2.real);

 cd3 = div_complexe_double(cd1,cd2);
 assert((double)474/1649 == cd3.real);
 assert((double)-145/1649 == cd3.imaginary);

 printf("test réussi\n");
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_complexe4");
  test_complexe4();
}