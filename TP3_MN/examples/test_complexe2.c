#include "complexe2.h"
#include "tests.h"

void test_complexe2 ()
{
 //complexe_float_t c1= {1.0, 2.0} ;
 //complexe_float_t c2= {3.0, 6.0} ;

 struct timeval start, end ;
 int i,j ;



 init_flop_micro () ;

 //c1 = add_complexe_float (c1, c2) ;
 // printf ("c1.r %f c1.i %f\n", c1.real, c1.imaginary) ;


 register complexe_double_t a  = (complexe_double_t) {10.0, 7.0} ;
 register complexe_double_t b  = (complexe_double_t) {25.0, 32.0} ;
 complexe_double_t cd1 ,cd2;


 float total_temp = 0.0;
  printf("réalisation de %d add complexe double sur %d itérations AVEC fonction inline\n\n" , NB_OPERATION, NB_FOIS_NORMAL);

 for(j = 0; j< NB_FOIS_NORMAL;j++){
   cd1 = a ;
   cd2 = b ;
   TOP_MICRO(start) ;
   for (i = 0 ; i < NB_OPERATION; i++)
     {
       cd1 = add_complexe_double (cd1, cd2) ;
     }
   TOP_MICRO(end) ;
   total_temp += tdiff_micro(&start,&end);
 }

 printf ("apres boucle cd1.real %f cd1.imaginary %f duree %f durée iteration %f\n", cd1.real, cd1.imaginary, total_temp , total_temp/NB_FOIS_NORMAL) ;

 calcul_flop_micro ("calcul complexe ", NB_FOIS_NORMAL*NB_OPERATION*2, total_temp) ;
}

void separator_test(char* message){
  printf("\n\n-----------------------------\n%s\n-----------------------------\n\n", message);
}

int main(int argc, char const *argv[]) {
  separator_test("test_complexe2");
  test_complexe2();
}