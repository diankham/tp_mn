#ifndef tests_include_vect
#define tests_include_vect
#include "complexe2.h"
#include "tests.h"

#define VECSIZE        65536
#define NB_FOIS_VEC    1000
#define MATRICE_SIZE   256

typedef complexe_double_t vcom_douple [VECSIZE] ;
typedef complexe_float_t vcom_float [VECSIZE] ;
typedef double vdouble [VECSIZE] ;
typedef float vfloat [VECSIZE] ;

inline void vector_init_com_double (vcom_douple V, complexe_double_t x){
  register unsigned int i ;
  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;
  return ;
}

inline void vector_init_com_float (vcom_float V, complexe_float_t x){
  register unsigned int i ;
  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;
  return ;
}

inline void vector_init_double (vdouble V, double x){
  register unsigned int i ;
  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;
  return ;
}

inline void vector_init_float (vfloat V, float x){
  register unsigned int i ;
  for (i = 0; i < VECSIZE; i++)
    V [i] = x ;
  return ;
}


inline void vector_print_float (vfloat V, int taille){
  register unsigned int i ;

  for (i = 0; i < taille; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  return ;
}

inline void vector_print_double (vdouble V, int taille){
  register unsigned int i ;

  for (i = 0; i < taille; i++)
    printf ("%f ", V[i]) ;
  printf ("\n") ;
  return ;
}


inline void vector_print_com_float (vcom_float V, int taille){
  register unsigned int i ;

  for (i = 0; i < taille; i++)
    printf ("%f %f , ", V[i].real, V[i].imaginary) ;
  printf ("\n") ;
  return ;
}


inline void vector_print_com_double (vcom_douple V, int taille){
  register unsigned int i ;

  for (i = 0; i < taille; i++)
    printf ("%f %f , ", V[i].real, V[i].imaginary) ;
  printf ("\n") ;
  return ;
}





typedef complexe_double_t mat_com_douple [MATRICE_SIZE*MATRICE_SIZE] ;
typedef complexe_float_t mat_com_float [MATRICE_SIZE*MATRICE_SIZE] ;
typedef double mat_double [MATRICE_SIZE*MATRICE_SIZE] ;
typedef float mat_float [MATRICE_SIZE*MATRICE_SIZE] ;



inline void matrice_init_com_double (mat_com_douple m, complexe_double_t x){
  register unsigned int i ;
  for (i = 0; i < MATRICE_SIZE*MATRICE_SIZE; i++)
    m[i] = x ;
  return ;
}

inline void matrice_init_com_float (mat_com_float m, complexe_float_t x){
  register unsigned int i ;
  for (i = 0; i < MATRICE_SIZE*MATRICE_SIZE; i++)
    m[i] = x ;
  return ;
}

inline void matrice_init_double (mat_double m, double x){
  register unsigned int i ;
  for (i = 0; i < MATRICE_SIZE*MATRICE_SIZE; i++)
    m[i] = x ;
  return ;
}

inline void matrice_init_float (mat_float m, float x){
  register unsigned int i ;
  for (i = 0; i < MATRICE_SIZE*MATRICE_SIZE; i++)
    m[i] = x ;
  return ;
}

#endif
