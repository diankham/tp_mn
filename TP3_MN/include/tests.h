#ifndef tests_include
#define tests_include

#include <stdio.h>
#include <stdlib.h>
#include "mnblas.h"
#include "flop.h"
#include <assert.h>

#define NB_FOIS_NORMAL    2048
#define NB_OPERATION        4096


inline void affiche_perf(int nb_iteration, double total_temp_timespec , unsigned long long int nb_operation ){
  printf("temps écoulé : %f     nb opération : %lli\n", total_temp_timespec, nb_operation);
  printf("une opération prend en moyenne %f secondes,  on obtient %5.3f GFlop/s\n\n", total_temp_timespec/nb_iteration , ((double) nb_operation / total_temp_timespec)/1000000000);
}
// a chaque opération on a 2 float (sachant que 1 float est a 8 octet). On prend donc le nombre d'opération * 16, divisé par le temps
inline void affiche_res_double(int nb_iteration, double total_temp_timespec, unsigned long long int nb_operation){
  printf("une opération prend en moyenne %f secondes,  on obtient %5.3f GB/s\n\n", total_temp_timespec/nb_iteration , ((((double) nb_operation/total_temp_timespec)/1000000000)));
}
// ((((double) nb_operation/total_temp_timespec)/1000000000)*8);

void test_complexe1();
void test_complexe2();
void test_complexe3();
void test_complexe4();
void test_copy();
void test_swap();
void test_dot();
void test_axpy();
void test_asum();
void test_amax();
void test_amin();
void test_nrm2 ();
void test_gemv();
void test_gemm();


void test_copy_open ();
void test_dot_open ();
void test_axpy_open ();
void test_gemv_open ();
void test_gemm_open ();

void separator_test(char* message);
void presente_var_global();
#endif
