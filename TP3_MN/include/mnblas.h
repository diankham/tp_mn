#include <stddef.h>
#define CBLAS_INDEX size_t  /* this may vary between platforms */

typedef enum {MNCblasRowMajor=101, MNCblasColMajor=102} MNCBLAS_LAYOUT;
typedef enum {MNCblasNoTrans=111, MNCblasTrans=112, MNCblasConjTrans=113} MNCBLAS_TRANSPOSE;
typedef enum {MNCblasUpper=121, MNCblasLower=122} MNCBLAS_UPLO;
typedef enum {MNCblasNonUnit=131, MNCblasUnit=132} MNCBLAS_DIAG;
typedef enum {MNCblasLeft=141, MNCblasRight=142} MNCBLAS_SIDE;

/*
 * ===========================================================================
 * Prototypes for level 1 BLAS functions
 * ===========================================================================
 */


/*
  BLAS copy
*/


void mncblas_scopy(const int N, const float *X, const int incX,
                 float *Y, const int incY);

void mncblas_dcopy(const int N, const double *X, const int incX,
                 double *Y, const int incY);


void mncblas_ccopy(const int N, const void *X, const int incX,
                 void *Y, const int incY);


void mncblas_zcopy(const int N, const void *X, const int incX,
                 void *Y, const int incY);


/*
  end COPY BLAS
*/



/*
  BLAS copy OPenMD
*/


void mncblas_scopy_open(const int N, const float *X, const int incX,
                 float *Y, const int incY);

void mncblas_dcopy_open(const int N, const double *X, const int incX,
                 double *Y, const int incY);

void mncblas_ccopy_open(const int N, const void *X, const int incX,
                       void *Y, const int incY);

void mncblas_zcopy_open(const int N, const void *X, const int incX,
                       void *Y, const int incY);

/*
  end COPY BLAS
*/




/*
  BLAS SWAP
*/

void mncblas_sswap(const int N, float *X, const int incX,
                 float *Y, const int incY);

void mncblas_dswap(const int N, double *X, const int incX,
                 double *Y, const int incY);

void mncblas_cswap(const int N, void *X, const int incX,
                 void *Y, const int incY);

void mncblas_zswap(const int N, void *X, const int incX,
                 void *Y, const int incY);

/*
  END SWAP
*/


/*

  BLAS DOT

*/

float  mncblas_sdot(const int N, const float  *X, const int incX,
                  const float  *Y, const int incY);

double mncblas_ddot(const int N, const double *X, const int incX,
                  const double *Y, const int incY);

void   mncblas_cdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_cdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_zdotu_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_zdotc_sub(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

/*
  END BLAS DOT
*/




/*

  BLAS DOT OPENMD

*/

float mncblas_sdot_open(const int N, const float *X, const int incX,
                 const float *Y, const int incY);

double mncblas_ddot_open(const int N, const double *X, const int incX,
                 const double *Y, const int incY);

void   mncblas_cdotu_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_cdotc_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

void   mncblas_zdotu_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotu);

void   mncblas_zdotc_sub_open(const int N, const void *X, const int incX,
                       const void *Y, const int incY, void *dotc);

/*
  END BLAS DOT
*/







/*
  BLAS AXPY
*/



void mnblas_saxpy(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY);

void mnblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);

void mnblas_caxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

void mnblas_zaxpy(const int N, const void *alpha, const void *X,
                 const int incX, void *Y, const int incY);

/*
  BLAS AXPY OPENMD
*/

void mnblas_saxpy_open (const int N,const float alpha, const float *X,
                    const int incX,  float *Y, const int incY );

void mnblas_daxpy_open (const int N,const double alpha, const double *X,
                    const int incX,  double *Y, const int incY );

void mnblas_caxpy_open (const int N,const void *alpha, const void *X,
                    const int incX,  void *Y, const int incY );

void mnblas_zaxpy_open (const int N,const void *alpha, const void *X,
                    const int incX,  void *Y, const int incY );


/*
 BLAS ASUM
*/

float  mnblas_sasum(const int N, const float *X, const int incX);

double mnblas_dasum(const int N, const double *X, const int incX);

float  mnblas_scasum(const int N, const void *X, const int incX);

double mnblas_dzasum(const int N, const void *X, const int incX);


/*
 BLAS AMIN
*/


CBLAS_INDEX mnblas_isamin(const int N, const float  *X, const int incX);

CBLAS_INDEX mnblas_idamin(const int N, const double *X, const int incX);

CBLAS_INDEX mnblas_icamin(const int N, const void   *X, const int incX);

CBLAS_INDEX mnblas_izamin(const int N, const void   *X, const int incX);



/*
 BLAS AMAX
*/

CBLAS_INDEX mnblas_isamax(const int N, const float  *X, const int incX);

CBLAS_INDEX mnblas_idamax(const int N, const double *X, const int incX);

CBLAS_INDEX mnblas_icamax(const int N, const void   *X, const int incX);

CBLAS_INDEX mnblas_izamax(const int N, const void   *X, const int incX);



/*
 BLAS NRM2
*/


float  mnblas_snrm2(const int N, const float *X, const int incX);

double mnblas_dnrm2(const int N, const double *X, const int incX);

float  mnblas_scnrm2(const int N, const void *X, const int incX);

double mnblas_dznrm2(const int N, const void *X, const int incX);




/*
 * ===========================================================================
 * Prototypes for level 1 BLAS routines
 * ===========================================================================
 */



/*
 * ===========================================================================
 * Prototypes for level 2 BLAS
 * ===========================================================================
 */



void mncblas_sgemv(const MNCBLAS_LAYOUT layout,
                 const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY);

                 //layout -> toujours ranger par ligne -> ROWMAJOR
                 //TRANSA -> toujours non
                 // lda -> inutile , a ne pas regarder -> cas simple
                 // M = N -> carrée



void mncblas_dgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);

void mncblas_cgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);

void mncblas_zgemv(MNCBLAS_LAYOUT layout,
                 MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                 const void *alpha, const void *A, const int lda,
                 const void *X, const int incX, const void *beta,
                 void *Y, const int incY);



/*
   BLAS GEMV OPENMD
*/

void mncblas_sgemv_open(const MNCBLAS_LAYOUT layout,//Y = alpha*a * X + b*y
                  const MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                  const float alpha, const float *A, const int lda,
                  const float *X, const int incX, const float beta,
                  float *Y, const int incY);

void mncblas_dgemv_open(MNCBLAS_LAYOUT layout,
                  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                  const double alpha, const double *A, const int lda,
                  const double *X, const int incX, const double beta,
                  double *Y, const int incY);

void mncblas_cgemv_open(MNCBLAS_LAYOUT layout,
                  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                  const void *alpha, const void *A, const int lda,
                  const void *X, const int incX, const void *beta,
                  void *Y, const int incY);

void mncblas_zgemv_open(MNCBLAS_LAYOUT layout,
                  MNCBLAS_TRANSPOSE TransA, const int M, const int N,
                  const void *alpha, const void *A, const int lda,
                  const void *X, const int incX, const void *beta,
                  void *Y, const int incY);


/*
 * ===========================================================================
 * Prototypes for level 3 BLAS
 * ===========================================================================
 */



void mncblas_sgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc);
                 // layout = toujouur row major
                 // TransA , TransB = toujours faux
                 // lda , ldb, ldc -> inutile , a ne pas regarder -> cas simple
                 // M = N = K
                 // C = alpha * a *b + beta*c


void mncblas_dgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);

void mncblas_cgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);

void mncblas_zgemm(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                 MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const void *alpha, const void *A,
                 const int lda, const void *B, const int ldb,
                 const void *beta, void *C, const int ldc);



/*
  BLAS GEMM OPENMD
*/


void mncblas_sgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                const int K, const float alpha, const float *A,
                const int lda, const float *B, const int ldb,
                const float beta, float *C, const int ldc);

void mncblas_dgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                const int K, const double alpha, const double *A,
                const int lda, const double *B, const int ldb,
                const double beta, double *C, const int ldc);

void mncblas_cgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                const int K, const void *alpha, const void *A,
                const int lda, const void *B, const int ldb,
                const void *beta, void *C, const int ldc);

void mncblas_zgemm_open(MNCBLAS_LAYOUT layout, MNCBLAS_TRANSPOSE TransA,
                MNCBLAS_TRANSPOSE TransB, const int M, const int N,
                const int K, const void *alpha, const void *A,
                const int lda, const void *B, const int ldb,
                const void *beta, void *C, const int ldc);





inline float abs_float(float f){
  return (f >= 0) ? f : -f;
}

inline double abs_double(double d){
  return (d >= 0) ? d : -d;
}
