
typedef struct {
  float real ;
  float imaginary ;
} complexe_float_t ;

typedef struct {
  double real ;
  double imaginary ;
} complexe_double_t ;



inline complexe_float_t add_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  r.real = c1.real + c2.real ;
  r.imaginary = c1.imaginary + c2.imaginary ;

  return r ;
}

inline complexe_double_t add_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;

  r.real = c1.real + c2.real ;
  r.imaginary = c1.imaginary + c2.imaginary ;

  return r ;
}



inline complexe_float_t mult_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;

  r.real = (c1.real * c2.real) - (c1.imaginary * c2.imaginary) ;
  r.imaginary =  (c1.real * c2.imaginary) + (c1.imaginary * c2.real);
  return r ;
}

inline complexe_double_t mult_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;

  r.real = (c1.real * c2.real) - (c1.imaginary * c2.imaginary) ;
  r.imaginary =  (c1.real * c2.imaginary) + (c1.imaginary * c2.real);

  return r ;
}


inline complexe_float_t div_complexe_float (const complexe_float_t c1, const complexe_float_t c2)
{
  complexe_float_t r ;
  complexe_float_t c2_compose = {c2.real, -c2.imaginary} ;
  complexe_float_t c2_compose_x_c1 = mult_complexe_float(c2_compose, c1);

  float diviseur = mult_complexe_float(c2,c2_compose).real;

  r.real =  c2_compose_x_c1.real / diviseur;
  r.imaginary = c2_compose_x_c1.imaginary / diviseur;

  return r ;
}

inline complexe_double_t div_complexe_double (const complexe_double_t c1, const complexe_double_t c2)
{
  complexe_double_t r ;
  complexe_double_t c2_compose = {c2.real, -c2.imaginary} ;
  complexe_double_t c2_compose_x_c1 = mult_complexe_double(c2_compose, c1);

  double diviseur = mult_complexe_double(c2,c2_compose).real;

  r.real =  c2_compose_x_c1.real / diviseur;
  r.imaginary = c2_compose_x_c1.imaginary / diviseur;

  return r ;
}
